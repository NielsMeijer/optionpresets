﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using Verse;

namespace NMeijer.OptionPresets
{
    [UsedImplicitly]
    public class OptionPresetsMod : Mod
    {
        
        public OptionPresetsMod(ModContentPack content) : base(content)
        {
        }

        #region Public Methods
        
        public override void DoSettingsWindowContents(Rect inRect)
        {
            Listing_Standard listingStandard = new Listing_Standard();
            listingStandard.Begin(inRect);

            Text.Font = GameFont.Medium;
            listingStandard.Label("OptionPresetsPresets".Translate());
            Text.Font = GameFont.Small;

            listingStandard.Gap();

            if(listingStandard.ButtonText("OptionPresetsLoadPreset".Translate()))
            {
                ShowFloatMenuOptionsForPresets(OptionPresets.LoadPreset);
            }
            if(listingStandard.ButtonText("OptionPresetsSaveAsPreset".Translate()))
            {
                Find.WindowStack.Add(new Dialog_SaveOptionsPreset());
            }

            listingStandard.Gap(6f);

            if(listingStandard.ButtonText("OptionPresetsDeletePreset".Translate()))
            {
                ShowFloatMenuOptionsForPresets(ConfirmPresetDeletion);
            }
            listingStandard.End();
        }

        public override string SettingsCategory()
        {
            return "OptionPresets".Translate();
        }

        #endregion
        
        #region Private Methods

        private static void ConfirmPresetDeletion(string preset)
        {
            Dialog_MessageBox confirmation = Dialog_MessageBox.CreateConfirmation("OptionPresetsDeleteConfirm".Translate(preset), () => OptionPresets.DeletePreset(preset));
            Find.WindowStack.Add(confirmation);
        }

        private static void ShowFloatMenuOptionsForPresets(Action<string> onClick)
        {
            string[] availablePresets = OptionPresets.GetAvailablePresets();

            List<FloatMenuOption> options = new List<FloatMenuOption>();
            for(int i = 0, length = availablePresets.Length; i < length; i++)
            {
                string preset = availablePresets[i];
                options.Add(new FloatMenuOption(preset, () => onClick(preset)));
            }

            Find.WindowStack.Add(new FloatMenu(options));
        }

        #endregion
    }
}